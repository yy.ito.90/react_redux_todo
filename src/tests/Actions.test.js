import * as actions from '../actions';

let nextTodoId = 0;
describe('actions', () => {
  it('ADD_TODO1', () => {
    const text = 'TODO1';
    const expectedAction = {
      type: actions.ADD_TODO,
      id: nextTodoId++,
      text,
    };
    expect(actions.addTodo(text)).toEqual(expectedAction);
  });

  it('ADD_TODO2', () => {
    const text = 'TODO2';
    const expectedAction = {
      type: actions.ADD_TODO,
      id: nextTodoId++,
      text,
    };
    expect(actions.addTodo(text)).toEqual(expectedAction);
  });

  it('TOGGLE_TODO1', () => {
    const id = 0;
    const expectedAction = {
      type: actions.TOGGLE_TODO,
      id,
    };
    expect(actions.toggleTodo(id)).toEqual(expectedAction);
  });

  it('SET_VISIBILITY_FILTER1', () => {
    const filter = 'SHOW_COMPLETED';
    const expectedAction = {
      type: actions.SET_VISIBILITY_FILTER,
      filter,
    };
    expect(actions.setVisibilityFilter(filter)).toEqual(expectedAction);
  });
});
