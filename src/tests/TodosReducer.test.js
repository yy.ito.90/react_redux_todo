import todos from '../reducers/todos';
import {
  ADD_TODO,
  TOGGLE_TODO,
} from '../actions';

describe('todos reducer', () => {
  it('initial', () => {
    const currentReducer = undefined;
    const dispatchedAction = {};
    const expectedReducer = [];
    expect(todos(currentReducer, dispatchedAction)).toEqual(expectedReducer);
  });

  it('ADD_TODO1', () => {
    const id = 0;
    const text = 'TODO1';

    const currentReducer = [];
    const dispatchedAction = {
      type: ADD_TODO,
      id,
      text,
    };
    const expectedReducer = [
      {
        id,
        text,
        completed: false,
      },
    ];

    expect(todos(currentReducer, dispatchedAction)).toEqual(expectedReducer);
  });

  it('TOGGLE_TODO1', () => {
    const id = 0;
    const text = 'TODO1';

    const currentReducer = [
      {
        id,
        text,
        completed: false,
      },
    ];
    const dispatchedAction = {
      type: TOGGLE_TODO,
      id,
    };
    const expectedReducer = [
      {
        id,
        text,
        completed: true,
      },
    ];

    expect(todos(currentReducer, dispatchedAction)).toEqual(expectedReducer);
  });

  it('ADD_TODO2', () => {
    const id1 = 0;
    const id2 = 1;
    const text1 = 'TODO1';
    const text2 = 'TODO2';

    const currentReducer = [
      {
        id: id1,
        text: text1,
        completed: true,
      },
    ];
    const dispatchedAction = {
      type: ADD_TODO,
      id: id2,
      text: text2,
    };
    const expectedReducer = [
      {
        id: id1,
        text: text1,
        completed: true,
      },
      {
        id: id2,
        text: text2,
        completed: false,
      },
    ];

    expect(todos(currentReducer, dispatchedAction)).toEqual(expectedReducer);
  });
});
