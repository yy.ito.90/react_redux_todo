import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import App from './components/App';
import configureStore from './store';

const store = configureStore();

// store.dispatch(setVisibilityFilter('SHOW_COMPLETED'));

render(
  <Provider store={store}>
      <App />
    </Provider>,
    document.getElementById('root'),
);
