import { connect } from 'react-redux';
import SendTodo from '../components/SendTodo';
import { addTodo } from '../actions';

const mapDispatchToProps = dispatch => ({
  onAddClick: (input) => {
    if (input.value !== '') {
      dispatch(addTodo(input.value));
      input.value = '';
    }
  },
});

const AddTodo = connect(
  null,
  mapDispatchToProps,
)(SendTodo);

export default AddTodo;
