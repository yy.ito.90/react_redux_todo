import React from 'react';
import PropTypes from 'prop-types';

const SendTodo = ({ onAddClick }) => {
  let input;

  return (
      <div>
          <input ref={(node) => {
              input = node;
            }}
            />
          <button onClick={() => onAddClick(input)}>
                AddTodo
            </button>
        </div>
  );
};

SendTodo.propTypes = {
  onAddClick: PropTypes.func.isRequired,
};

export default SendTodo;
